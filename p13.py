#!/usr/bin/env python3

import p

@p.puzzle(13, 1)
def main(lines):
  dots = set()
  folds = []
  for line in lines:
    if line.startswith("fold"):
      folds.append(line)
    elif line:
      dots.add(tuple(map(int, line.split(","))))

  for fold_index, fold in enumerate(folds, 1):
    axis = fold[11]
    number = int(fold[13:])
    for x, y in tuple(dots):
      if axis == "x" and x > number:
        dots.remove((x, y))
        dots.add((2 * number - x, y))
      elif axis == "y" and y > number:
        dots.remove((x, y))
        dots.add((x, 2 * number - y))
    if fold_index == 1:
      yield len(dots)

  xs = {x for x, y in dots}
  ys = {y for x, y in dots}

  x0, x1 = min(xs), max(xs)
  y0, y1 = min(ys), max(ys)

  result = "\n"
  for y in range(y0, y1 + 1):
    for x in range(x0, x1 + 1):
      if (x, y) in dots:
        result += "#"
      else:
        result += " "
    result += "\n"
  yield result

if __name__ == "__main__":
  p.main()
