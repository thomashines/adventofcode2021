#!/usr/bin/env python3

import p

DIGIT_TO_SEGMENTS = {
  0: "abcefg",
  1: "cf",
  2: "acdeg",
  3: "acdfg",
  4: "bcdf",
  5: "abdfg",
  6: "abdefg",
  7: "acf",
  8: "abcdefg",
  9: "abcdfg",
}

SEGMENTS_TO_DIGITS = {segments: digit for digit, segments in DIGIT_TO_SEGMENTS.items()}

@p.puzzle(8, 1)
def main(lines):
  count_lengths = set()
  for digit in (1, 4, 7, 8):
    count_lengths.add(len(DIGIT_TO_SEGMENTS[digit]))
  count = 0
  for line in lines:
    right = line.split(" | ")[1]
    for digit in right.split(" "):
      if len(digit) in count_lengths:
        count += 1
  yield count

  true_rates = {}
  for segments in DIGIT_TO_SEGMENTS.values():
    for segment in segments:
      if segment in true_rates:
        true_rates[segment] += 1
      else:
        true_rates[segment] = 1

  true_rates_counts = tuple(sorted(true_rates.values()))
  unique_rates = tuple(sorted(filter(lambda rate: true_rates_counts.count(rate[1]) == 1, true_rates.items())))

  right_sum = 0
  for line in lines:
    left, right = line.split(" | ")

    mapped_rates = {}
    for segments in left.split(" "):
      for segment in segments:
        if segment in mapped_rates:
          mapped_rates[segment] += 1
        else:
          mapped_rates[segment] = 1

    mapping_to_true = {}
    mapping_to_mapped = {}

    for true_segment, true_count in unique_rates:
      for mapped_segment, mapped_count in mapped_rates.items():
        if true_count == mapped_count:
          mapping_to_true[mapped_segment] = true_segment
          mapping_to_mapped[true_segment] = mapped_segment

    for digit in (4, 7):
      true_segments = DIGIT_TO_SEGMENTS[digit]
      for mapped_segments in left.split(" "):
        if len(true_segments) == len(mapped_segments):
          for mapped_segment in mapped_segments:
            if mapped_segment not in mapping_to_true:
              for true_segment in true_segments:
                if true_segment not in mapping_to_mapped:
                  if mapped_rates[mapped_segment] == true_rates[true_segment]:
                    mapping_to_true[mapped_segment] = true_segment
                    mapping_to_mapped[true_segment] = mapped_segment

    for mapped_segment in mapped_rates.keys():
      if mapped_segment not in mapping_to_true:
        for true_segment in true_rates.keys():
          if true_segment not in mapping_to_mapped:
            mapping_to_true[mapped_segment] = true_segment
            mapping_to_mapped[true_segment] = mapped_segment

    right_number = ""
    for mapped_segments in right.split(" "):
      true_segments = ""
      for mapped_segment in mapped_segments:
        true_segments += mapping_to_true[mapped_segment]
      true_segments = "".join(sorted(true_segments))
      right_number += str(SEGMENTS_TO_DIGITS[true_segments])
    right_sum += int(right_number)

  yield right_sum


if __name__ == "__main__":
  p.main()
