#!/usr/bin/env python3

import p

@p.puzzle(5, 1)
def main(lines):
  map_square = {}
  map_diagonal = {}
  for line in lines:
    start, end = line.split(" -> ")
    x0, y0 = map(int, start.split(","))
    x1, y1 = map(int, end.split(","))
    dx = 0 if x0 == x1 else (1 if x0 < x1 else -1)
    dy = 0 if y0 == y1 else (1 if y0 < y1 else -1)
    x1, y1 = x1 + dx, y1 + dy
    x, y = x0, y0
    while x != x1 or y != y1:
      if dx == 0 or dy == 0:
        if (x, y) not in map_square:
          map_square[x, y] = 1
        else:
          map_square[x, y] += 1
      if (x, y) not in map_diagonal:
        map_diagonal[x, y] = 1
      else:
        map_diagonal[x, y] += 1
      x, y = x + dx, y + dy
  for map_count in (map_square, map_diagonal):
    dangers = 0
    for count in map_count.values():
      if count >= 2:
        dangers += 1
    yield dangers

if __name__ == "__main__":
  p.main()
