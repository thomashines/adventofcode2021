#!/usr/bin/env python3

import p

@p.puzzle(2, 1)
def main(lines):
  horizontal = 0
  depth = 0
  for line in lines:
    direction, count = line.split(" ")
    count = int(count)
    if direction == "forward":
      horizontal += count
    elif direction == "up":
      depth -= count
    elif direction == "down":
      depth += count
  yield horizontal * depth

  aim = 0
  horizontal = 0
  depth = 0
  for line in lines:
    direction, count = line.split(" ")
    count = int(count)
    if direction == "forward":
      horizontal += count
      depth += aim * count
    elif direction == "up":
      aim -= count
    elif direction == "down":
      aim += count
  yield horizontal * depth

if __name__ == "__main__":
  p.main()
