#!/usr/bin/env python3

import itertools

import p

@p.puzzle(25, 1)
def main(lines):
  easts = set()
  souths = set()
  size = [0, 0]
  for y, line in enumerate(lines):
    for x, character in enumerate(line):
      if character == ">":
        easts.add((x, y))
      elif character == "v":
        souths.add((x, y))
      size[0] = max(size[0], x + 1)
      size[1] = max(size[1], y + 1)

  for step in itertools.count(1):

    changed = False

    next_easts = set()
    for x, y in easts:
      xx, yy = (x + 1) % size[0], y
      if ((xx, yy) in easts) or ((xx, yy) in souths):
        next_easts.add((x, y))
      else:
        next_easts.add((xx, yy))
        changed = True
    easts = next_easts

    next_souths = set()
    for x, y in souths:
      xx, yy = x, (y + 1) % size[1]
      if ((xx, yy) in easts) or ((xx, yy) in souths):
        next_souths.add((x, y))
      else:
        next_souths.add((xx, yy))
        changed = True
    souths = next_souths

    if not changed:
      yield step
      break

if __name__ == "__main__":
  p.main()
