#!/usr/bin/env python3

import p

def parse(string):
  if string[0] != "[":
    return int(string)
  depth = 0
  for index, character in enumerate(string):
    if character == "[":
      depth += 1
    elif character == "]":
      depth -= 1
    elif depth == 1 and character == ",":
      return (parse(string[1:index]), parse(string[index + 1:-1]))
  return None

def find_nested_4(number, depth=0):
  if type(number) is tuple:
    if depth >= 4:
      return ()
    left = find_nested_4(number[0], depth + 1)
    if left is not None:
      return (0,) + left
    right = find_nested_4(number[1], depth + 1)
    if right is not None:
      return (1,) + right
  return None

def find_ge_10(number):
  if type(number) is tuple:
    left = find_ge_10(number[0])
    if left is not None:
      return (0,) + left
    right = find_ge_10(number[1])
    if right is not None:
      return (1,) + right
  elif number >= 10:
    return ()
  return None

def indices(number):
  if type(number) is tuple:
    for index in indices(number[0]):
      yield (0,) + index
    for index in indices(number[1]):
      yield (1,) + index
  else:
    yield ()

def add_value(number, index, value):
  if index == (0,):
    return (number[0] + value, number[1])
  if index == (1,):
    return (number[0], number[1] + value)
  if index[0] == 0:
    return (add_value(number[0], index[1:], value), number[1])
  if index[0] == 1:
    return (number[0], add_value(number[1], index[1:], value))
  return None

def replace(number, index, value):
  if index == ():
    return value
  if index[0] == 0:
    return (replace(number[0], index[1:], value), number[1])
  if index[0] == 1:
    return (number[0], replace(number[1], index[1:], value))
  return None

def get(number, index):
  if index == ():
    return number
  return get(number[index[0]], index[1:])

def explode(number, index):
  left_index, right_index = index + (0,), index + (1,)
  left, right = get(number, left_index), get(number, right_index)
  indices_before = tuple(indices(number))
  left_index_index = indices_before.index(left_index)
  right_index_index = indices_before.index(right_index)
  result = number
  if (left_index_index - 1) >= 0:
    result = add_value(result, indices_before[left_index_index - 1], left)
  if (right_index_index + 1) < len(indices_before):
    result = add_value(result, indices_before[right_index_index + 1], right)
  result = replace(result, index, 0)
  return result

def split(number, index):
  value = get(number, index)
  left = value // 2
  right = value - left
  return replace(number, index, (left, right))

def reduce(number):
  result = number
  while True:
    to_explode = find_nested_4(result)
    if to_explode is not None:
      result = explode(result, to_explode)
    else:
      to_split = find_ge_10(result)
      if to_split is not None:
        result = split(result, to_split)
      else:
        return result

def add(left, right):
  return reduce((left, right))

def magnitude(number):
  if type(number) is tuple:
    return 3 * magnitude(number[0]) + 2 * magnitude(number[1])
  return number

@p.puzzle(18, 1)
def main(lines):
  numbers = tuple(map(parse, lines))

  total = reduce(numbers[0])
  for number in numbers[1:]:
    total = add(total, number)
  yield magnitude(total)

  best_magnitude = 0
  for left_index, left in enumerate(numbers):
    for right_index, right in enumerate(numbers):
      if left_index == right_index:
        continue
      this_magnitude = magnitude(add(left, right))
      if this_magnitude > best_magnitude:
        best_magnitude = this_magnitude
  yield best_magnitude

if __name__ == "__main__":
  p.main()
