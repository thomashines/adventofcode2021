#!/usr/bin/env python3

import p

@p.puzzle(9, 1)
def main(lines):
  grid = {}
  for y, row in enumerate(lines):
    for x, height in enumerate(row):
      grid[x, y] = int(height)

  lows = set()
  for (x, y), height in grid.items():
    islow = True
    for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
      adjacent = grid.get((x + dx, y + dy))
      if adjacent is not None and adjacent <= height:
        islow = False
        break
    if islow:
      lows.add((x, y))

  yield sum(grid[point] + 1 for point in lows)

  basins = {}
  for low in lows:
    basins[low] = set()
    queue = set((low,))
    while queue:
      point = queue.pop()
      basins[low].add(point)
      point_height = grid[point]
      x, y = point
      for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
        adjacent = (x + dx, y + dy)
        if adjacent not in grid:
          continue
        if adjacent in basins[low]:
          continue
        if adjacent in queue:
          continue
        adjacent_height = grid[adjacent]
        if adjacent_height == 9:
          continue
        if adjacent_height > point_height:
          queue.add(adjacent)

  sorted_basins = sorted(basins.items(), key=lambda basin: -len(basin[1]))
  yield p.product(len(basin[1]) for basin in sorted_basins[:3])


if __name__ == "__main__":
  p.main()
