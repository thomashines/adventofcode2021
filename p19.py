#!/usr/bin/env python3

import collections
import itertools

import p

def rotations(x, y, z):
  return (
    (-x, -y, z),
    (-x, -z, -y),
    (-x, y, -z),
    (-x, z, y),
    (-y, -x, -z),
    (-y, -z, x),
    (-y, x, z),
    (-y, z, -x),
    (-z, -x, y),
    (-z, -y, -x),
    (-z, x, -y),
    (-z, y, x),
    (x, -y, -z),
    (x, -z, y),
    (x, y, z),
    (x, z, -y),
    (y, -x, z),
    (y, -z, -x),
    (y, x, -z),
    (y, z, x),
    (z, -x, -y),
    (z, -y, x),
    (z, x, y),
    (z, y, -x),
  )

def vector_sub(left, right):
  return (left[0] - right[0], left[1] - right[1], left[2] - right[2])

def vector_add(left, right):
  return (left[0] + right[0], left[1] + right[1], left[2] + right[2])

@p.puzzle(19, 1)
def main(lines):
  scanner = []
  scanners = []
  for line in lines:
    if line.startswith("---"):
      if scanner:
        scanners.append(tuple(sorted(scanner)))
        scanner = []
    elif line:
      scanner.append(tuple(map(int, line.split(","))))
  if scanner:
    scanners.append(tuple(sorted(scanner)))
  scanners = tuple(scanners)

  scanners_orientations_beacons = collections.defaultdict(lambda: collections.defaultdict(lambda: []))
  for scanner_index, scanner in enumerate(scanners):
    for beacon in scanner:
      for orientation_index, orientation in enumerate(rotations(*beacon)):
        scanners_orientations_beacons[scanner_index][orientation_index].append(orientation)
  scanners_orientations_beacons = tuple(
    tuple(
      tuple(
        sorted(
          scanners_orientations_beacons[s][o]
        )
      ) for o in range(24)
    ) for s in range(len(scanners))
  )

  scanners_orientations_diffs = collections.defaultdict(lambda: collections.defaultdict(lambda: set()))
  for scanner_index, orientations_beacons in enumerate(scanners_orientations_beacons):
    for orientation_index, beacons in enumerate(orientations_beacons):
      for left, right in itertools.product(beacons, repeat=2):
        scanners_orientations_diffs[scanner_index][orientation_index].add(vector_sub(left, right))
  scanners_orientations_diffs = tuple(
    tuple(
      scanners_orientations_diffs[s][o] for o in range(24)
    ) for s in range(len(scanners))
  )

  orientations = {0: 0}
  offsets = {0: (0, 0, 0)}
  localised = {0: scanners_orientations_beacons[0][0]}
  while len(localised) < len(scanners):
    for scanner_index in range(len(scanners)):
      if scanner_index in localised:
        continue
      for orientation_index in range(24):
        beacons = scanners_orientations_beacons[scanner_index][orientation_index]
        diffs = scanners_orientations_diffs[scanner_index][orientation_index]
        beacon_to_localised = None
        matches = 0
        for localised_scanner_index in localised.keys():
          localised_beacons = localised[localised_scanner_index]
          localised_diffs = scanners_orientations_diffs[localised_scanner_index][orientations[localised_scanner_index]]
          beacon_to_localised = None
          matches = 0
          if len(diffs & localised_diffs) <= 64:
            continue
          for beacon, localised_beacon in itertools.product(beacons, localised_beacons):
            beacon_to_localised = vector_sub(localised_beacon, beacon)
            matches = 0
            for beacon_check in beacons:
              match = False
              for localised_beacon_check in localised_beacons:
                if vector_add(beacon_check, beacon_to_localised) == localised_beacon_check:
                  match = True
                  break
              if match:
                matches += 1
                if matches >= 12:
                  break
            if matches >= 12:
                  break
          if matches >= 12:
                  break
        if matches >= 12:
          orientations[scanner_index] = orientation_index
          offsets[scanner_index] = beacon_to_localised
          localised[scanner_index] = tuple(map(lambda beacon: vector_add(beacon, beacon_to_localised), beacons))
          break

  beacons = set()
  for localised_beacons in localised.values():
    beacons.update(localised_beacons)
  yield len(beacons)

  max_dist = 0
  for left, right in itertools.combinations(offsets.values(), 2):
    diff = vector_sub(left, right)
    dist = abs(diff[0]) + abs(diff[1]) + abs(diff[2])
    max_dist = max(max_dist, dist)
  yield max_dist

if __name__ == "__main__":
  p.main()
