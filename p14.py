#!/usr/bin/env python3

import collections
import itertools

import p

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)

CACHE = {}
def get_final_counts_between(rules, polymer, remaining_steps):
  if remaining_steps == 0:
    return {}
  if (polymer, remaining_steps) in CACHE:
    return CACHE[polymer, remaining_steps]
  counts = collections.defaultdict(lambda: 0)
  left, right = polymer
  middle = rules.get(polymer)
  if middle is not None:
    counts[middle] += 1
    for element, count in get_final_counts_between(rules, left + middle, remaining_steps - 1).items():
      counts[element] += count
    for element, count in get_final_counts_between(rules, middle + right, remaining_steps - 1).items():
      counts[element] += count
  CACHE[polymer, remaining_steps] = counts
  return counts

@p.puzzle(14, 1)
def main(lines):
  global CACHE
  for key in tuple(CACHE.keys()):
    del CACHE[key]

  polymer = lines[0]
  rules = {}
  for line in lines[2:]:
    left, right = line.split(" -> ")
    rules[left] = right

  for steps in (10, 40):
    counts = collections.defaultdict(lambda: 0)
    for left, right in pairwise(str(polymer)):
      counts[left] += 1
      for element, count in get_final_counts_between(rules, left + right, steps).items():
        counts[element] += count
    counts[polymer[-1]] += 1
    counts = tuple(sorted((count, element) for element, count in counts.items()))
    yield counts[-1][0] - counts[0][0]

if __name__ == "__main__":
  p.main()
