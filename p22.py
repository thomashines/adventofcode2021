#!/usr/bin/env python3

import collections
import itertools

import p

def parse_range(string):
  left, right = map(int, string[2:].split(".."))
  return min(left, right), max(left, right) + 1

def test_range(rule_range, value):
  return rule_range[0] <= value < rule_range[1]

def test_rule(rule, x, y, z):
  if (test_range(rule[0], x) and test_range(rule[1], y) and
      test_range(rule[2], z)):
    return rule[3]
  return None

def intersects(left_low, left_high, right_low, right_high):
  return not (
    (left_high[0] <= right_low[0]) or
    (left_high[1] <= right_low[1]) or
    (left_high[2] <= right_low[2]) or
    (right_high[0] <= left_low[0]) or
    (right_high[1] <= left_low[1]) or
    (right_high[2] <= left_low[2])
  )

@p.puzzle(22, 1)
def main(lines):
  rules = []
  for line in lines:
    state, ranges = line.split(" ")
    state = True if (state == "on") else False
    ranges = tuple(map(parse_range, ranges.split(",")))
    rules.append(ranges + (state,))

  index0 = 0
  while index0 < len(rules):
    rule0 = rules[index0]
    low = (rule0[0][0], rule0[1][0], rule0[2][0])
    high = (rule0[0][1], rule0[1][1], rule0[2][1])
    relevant = True
    index1 = index0 + 1
    while index1 < len(rules):
      rule1 = rules[index1]
      if ((test_rule(rule1, *low) is not None) and
          (test_rule(rule1, *high) is not None)):
        relevant = False
        break
      index1 += 1
    if not relevant:
      rules.pop(index0)
    else:
      index0 += 1

  rules = tuple(rules)

  intersections = [[] for _ in range(len(rules))]
  for rule_index0 in range(len(rules)):
    rule0 = rules[rule_index0]
    low0 = (rule0[0][0], rule0[1][0], rule0[2][0])
    high0 = (rule0[0][1], rule0[1][1], rule0[2][1])
    for rule_index1 in range(len(rules)):
      if rule_index0 == rule_index1:
        continue
      rule1 = rules[rule_index1]
      low1 = (rule1[0][0], rule1[1][0], rule1[2][0])
      high1 = (rule1[0][1], rule1[1][1], rule1[2][1])
      if intersects(low0, high0, low1, high1):
        intersections[rule_index0].append(rule_index1)
  intersections = tuple(map(tuple, intersections))

  rules_regions = [() for _ in range(len(rules))]
  for new_rule_index in range(len(rules)):
    new_rule = rules[new_rule_index]
    new_xs = new_rule[0]
    new_ys = new_rule[1]
    new_zs = new_rule[2]
    new_low = (new_xs[0], new_ys[0], new_zs[0])
    new_high = (new_xs[1], new_ys[1], new_zs[1])
    for old_rule_index in intersections[new_rule_index]:
      if len(rules_regions[old_rule_index]) == 0:
        continue
      old_rule = rules[old_rule_index]
      new_old_regions = set()
      for old_low, old_high in rules_regions[old_rule_index]:
        if intersects(new_low, new_high, old_low, old_high):
          xs = tuple(sorted(set(new_xs + (old_low[0], old_high[0]))))
          ys = tuple(sorted(set(new_ys + (old_low[1], old_high[1]))))
          zs = tuple(sorted(set(new_zs + (old_low[2], old_high[2]))))
          for x0, x1 in itertools.pairwise(xs):
            for y0, y1 in itertools.pairwise(ys):
              for z0, z1 in itertools.pairwise(zs):
                if test_rule(new_rule, x0, y0, z0) is not None:
                  continue
                if intersects(old_low, old_high, (x0, y0, z0), (x1, y1, z1)):
                  new_old_regions.add(((x0, y0, z0), (x1, y1, z1)))
        else:
          new_old_regions.add((old_low, old_high))
      new_old_regions = list(sorted(new_old_regions))
      region0_index = 0
      while (region0_index + 1) < len(new_old_regions):
        region0 = new_old_regions[region0_index]
        xs0 = (region0[0][0], region0[1][0])
        ys0 = (region0[0][1], region0[1][1])
        zs0 = (region0[0][2], region0[1][2])

        region1 = new_old_regions[region0_index + 1]
        xs1 = (region1[0][0], region1[1][0])
        ys1 = (region1[0][1], region1[1][1])
        zs1 = (region1[0][2], region1[1][2])

        coalesce = (((xs0 == xs1) and (ys0 == ys1) and ((zs0[1] == zs1[0]) or (zs0[0] == zs1[1]))) or
                    ((xs0 == xs1) and (zs0 == zs1) and ((ys0[1] == ys1[0]) or (ys0[0] == ys1[1]))) or
                    ((ys0 == ys1) and (zs0 == zs1) and ((xs0[1] == xs1[0]) or (xs0[0] == xs1[1]))))
        if coalesce:
          coalesced_xs = (min(xs0 + xs1), max(xs0 + xs1))
          coalesced_ys = (min(ys0 + ys1), max(ys0 + ys1))
          coalesced_zs = (min(zs0 + zs1), max(zs0 + zs1))
          coalesced_region = (
            (coalesced_xs[0], coalesced_ys[0], coalesced_zs[0]),
            (coalesced_xs[1], coalesced_ys[1], coalesced_zs[1]),
          )
          new_old_regions[region0_index] = coalesced_region
          new_old_regions.pop(region0_index + 1)
          if region0_index > 0:
            region0_index -= 1
        else:
          region0_index += 1
      rules_regions[old_rule_index] = tuple(sorted(new_old_regions))
    if new_rule[3]:
      rules_regions[new_rule_index] = ((new_low, new_high),)
    else:
      rules_regions[new_rule_index] = ()

  ons_50 = 0
  ons_all = 0
  for rule_index, regions in enumerate(rules_regions):
    rule = rules[rule_index]
    if rule[3]:
      for low, high in regions:

        ons_all += (high[0] - low[0]) * (high[1] - low[1]) * (high[2] - low[2])

        low = (max(-50, low[0]), max(-50, low[1]), max(-50, low[2]))
        high = (min(51, high[0]), min(51, high[1]), min(51, high[2]))
        if (low[0] < high[0]) and (low[1] < high[1]) and (low[2] < high[2]):
          ons_50 += (high[0] - low[0]) * (high[1] - low[1]) * (high[2] - low[2])

  yield ons_50
  yield ons_all

if __name__ == "__main__":
  p.main()
