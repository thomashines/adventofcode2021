#!/usr/bin/env python3

import heapq

import p

ADJACENTS = (
  (-1, 0),
  (1, 0),
  (0, -1),
  (0, 1),
)

def search(grid, goal):
  opened = set()
  queue = []
  heapq.heappush(queue, (0, 0, 0, 0))
  while queue:
    _, risk, x, y = heapq.heappop(queue)
    if (x, y) == goal:
      return risk
    for dx, dy in ADJACENTS:
      succ_x, succ_y = x + dx, y + dy
      if (succ_x, succ_y) not in grid:
        continue
      if (succ_x, succ_y) in opened:
        continue
      succ_risk = risk + grid[succ_x, succ_y]
      to_go = max(abs(goal[0] - succ_x), abs(goal[1] - succ_y))
      heapq.heappush(queue, (succ_risk + to_go, succ_risk, succ_x, succ_y))
      opened.add((succ_x, succ_y))
  return None

@p.puzzle(15, 1)
def main(lines):
  grid = {}
  for y, row in enumerate(lines):
    for x, risk in enumerate(row):
      grid[x, y] = int(risk)
  size = (len(lines[0]), len(lines))
  goal = (size[0] - 1, size[1] - 1)
  yield search(grid, goal)

  for (x, y), risk in tuple(grid.items()):
    for ry in range(1, 5):
      yy = ry * size[1] + y
      for rx in range(5):
        xx = rx * size[0] + x
        grid[xx, yy] = ((int(risk) + rx + ry - 1) % 9) + 1
  size = (size[0] * 5, size[1] * 5)
  goal = (size[0] - 1, size[1] - 1)
  yield search(grid, goal)

if __name__ == "__main__":
  p.main()
