#!/usr/bin/env python3

import p

MAP_LR = {
  "(": ")",
  "[": "]",
  "{": "}",
  "<": ">",
}
MAP_RL = {right: left for left, right in MAP_LR.items()}
ERROR_SCORES = {
  ")": 3,
  "]": 57,
  "}": 1197,
  ">": 25137,
}
COMPLETION_SCORES = {
  ")": 1,
  "]": 2,
  "}": 3,
  ">": 4,
}

@p.puzzle(10, 1)
def main(lines):
  error_score = 0
  completion_scores = []
  for line in lines:
    completion = ""
    is_valid = True
    for character in line:
      if character in MAP_LR:
        completion = MAP_LR[character] + completion
      else:
        if completion[0] == character:
          completion = completion[1:]
        else:
          is_valid = False
          error_score += ERROR_SCORES[character]
          break
    if is_valid and completion:
      completion_score = 0
      for character in completion:
        completion_score *= 5
        completion_score += COMPLETION_SCORES[character]
      completion_scores.append(completion_score)
  yield error_score

  completion_scores.sort()
  yield completion_scores[len(completion_scores) // 2]

if __name__ == "__main__":
  p.main()
