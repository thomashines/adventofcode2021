#!/usr/bin/env python3

import p

def solve(start, cost):
  best_goal = None
  best_cost = None
  for goal in range(min(start), max(start) + 1):
    total_cost = 0
    for position in start:
      total_cost += cost(goal, position)
      if best_cost is not None and total_cost >= best_cost:
        break
    if best_cost is None or total_cost < best_cost:
      best_cost = total_cost
      best_goal = goal
  return best_cost

@p.puzzle(7, 1)
def main(lines):
  start = tuple(map(int, lines[0].split(",")))
  yield solve(start, lambda goal, position: abs(goal - position))
  yield solve(start, lambda goal, position: ((abs(goal - position) + 1) * abs(goal - position)) // 2)

if __name__ == "__main__":
  p.main()
