#!/usr/bin/env python3

import collections
import functools
import heapq
import itertools
import random
import time

import p

COSTS = (1, 10, 100, 1000)
GOAL_STATE = (
  "#############",
  "#...........#",
  "###A#B#C#D###",
  "  #A#B#C#D#  ",
  "  #########  ",
)
HALLWAY_Y = 1
ROOM_XS = (3, 5, 7, 9)
ROOM_YS = (2, 3, 4, 5)

def sort_state(state):
  return tuple(tuple(sorted(positions)) for positions in state)

def parse_state(lines):
  state = [[] for _ in "ABCD"]
  for y, line in enumerate(lines):
    for x, character in enumerate(line):
      if character in "ABCD":
        state["ABCD".index(character)].append((x, y))
  return sort_state(state)

def heuristic(goal, state):
  cost = 0
  for type_index, positions in enumerate(state):
    for piece_index, position in enumerate(positions):
      if position[0] == ROOM_XS:
        if position[1] == HALLWAY_Y:
          cost += COSTS[type_index]
      else:
        cost += COSTS[type_index] * abs(ROOM_XS[type_index] - position[0])
        if position[1] == HALLWAY_Y:
          cost += COSTS[type_index]
        else:
          cost += COSTS[type_index] * (abs(HALLWAY_Y - position[1]) + 1)
  return cost

def reachables(spaces, state, start):
  occupied = functools.reduce(lambda o, p: o | set(p), state, set())
  reachable = {}
  queue = []
  heapq.heappush(queue, (0, start))
  while queue:
    steps, (x, y) = heapq.heappop(queue)
    if (x, y) in reachable:
      continue
    reachable[x, y] = steps
    for dx, dy in ((0, -1), (0, 1), (-1, 0), (1, 0)):
      xx, yy = x + dx, y + dy
      if (((xx, yy) not in spaces) or ((xx, yy) in occupied) or
          ((xx, yy) in reachable)):
        continue
      heapq.heappush(queue, (steps + 1, (xx, yy)))
  return reachable

def neighbours(spaces, state):
  room_is_done = [False for _ in range(4)]
  room_is_pure = [True for _ in range(4)]
  for type_index, positions in enumerate(state):
    done_count = 0
    for position in positions:
      if position[1] != HALLWAY_Y:
        room_index = ROOM_XS.index(position[0])
        if type_index == room_index:
          done_count += 1
        else:
          room_is_pure[room_index] = False
    if done_count == len(ROOM_YS):
      room_is_done[type_index] = True

  options = []

  for type_index, positions in enumerate(state):
    if room_is_done[type_index]:
      continue
    for piece_index, position in enumerate(positions):
      next_state = list(map(list, state))
      is_in_hallway = position[1] == HALLWAY_Y
      is_in_right_room = not is_in_hallway and (position[0] == ROOM_XS[type_index])
      is_in_wrong_room = not is_in_hallway and (position[0] != ROOM_XS[type_index])
      for (x, y), steps in reachables(spaces, state, position).items():
        # Never move between hallway positions
        will_be_in_hallway = y == HALLWAY_Y
        if is_in_hallway and will_be_in_hallway:
          continue

        # Never move to an intersection
        will_be_in_intersection = will_be_in_hallway and (x in ROOM_XS)
        if will_be_in_intersection:
          continue

        # Never move to a position in the wrong room
        will_be_in_wrong_room = not will_be_in_hallway and (x != ROOM_XS[type_index])
        if will_be_in_wrong_room:
          continue

        # Never move to a position in the right room if it is not pure
        will_be_in_right_room = not will_be_in_hallway and (x == ROOM_XS[type_index])
        if will_be_in_right_room and not room_is_pure[type_index]:
          continue

        # Allow this move
        next_state[type_index][piece_index] = (x, y)
        option = steps * COSTS[type_index], sort_state(next_state)

        # If this is a move further into the right room then always just do it
        if (will_be_in_right_room and
            ((not is_in_right_room) or
             (is_in_right_room and (y > position[1])))):
          yield option
          return

        options.append(option)

  yield from options

def show(spaces, state):
  for y in range(7):
    line = ""
    for x in range(13):
      if (x, y) not in spaces:
        line += "#"
      elif (x, y) in state[0]:
        line += "A"
      elif (x, y) in state[1]:
        line += "B"
      elif (x, y) in state[2]:
        line += "C"
      elif (x, y) in state[3]:
        line += "D"
      else:
        line += "."
    print(line)

def solve(start, goal):
  spaces = set()
  for y, line in enumerate(start):
    for x, character in enumerate(line):
      if character in ".ABCD":
        spaces.add((x, y))

  start = parse_state(start)
  goal = parse_state(goal)

  # best_heuristic = heuristic(goal, start)
  visited = set()
  queue = []
  heapq.heappush(queue, (heuristic(goal, start), 0, start))
  while queue:
    _, cost, state = heapq.heappop(queue)
    if state in visited:
      continue
    visited.add(state)
    if state == goal:
      return cost
    # if heuristic(goal, state) < best_heuristic:
    #   best_heuristic = heuristic(goal, state)
    #   show(spaces, state)
    for next_cost, next_state in neighbours(spaces, state):
      if next_state in visited:
        continue
      heapq.heappush(queue, (cost + next_cost + heuristic(goal, next_state), cost + next_cost, next_state))

@p.puzzle(23, 1)
def main(lines):
  start = list(lines)
  goal = list(GOAL_STATE)
  yield solve(start, goal)

  start.insert(3, "  #D#C#B#A#  ")
  start.insert(4, "  #D#B#A#C#  ")
  goal.insert(3, "  #A#B#C#D#  ")
  goal.insert(4, "  #A#B#C#D#  ")
  yield solve(start, goal)

if __name__ == "__main__":
  p.main()
