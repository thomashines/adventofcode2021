#!/usr/bin/env python3

import p

@p.puzzle(4, 1)
def main(lines):
  calls = tuple(map(int, lines[0].split(",")))

  boards = []
  board = []
  for line in lines[1:]:
    if line:
      row = []
      for number in line.split(" "):
        if number:
          row.append(int(number))
      board.append(row)
    elif board:
      boards.append(board)
      board = []
  if board:
    boards.append(board)

  lines = []
  for board_index, board in enumerate(boards):
    for row in board:
      lines.append((board_index, set(row)))
    for col in range(len(board[0])):
      line = set()
      for row in range(len(board)):
        line.add(board[row][col])
      lines.append((board_index, line))

  remaining = set(range(len(boards)))
  first_bingo = None
  last_bingo = None
  call_index = 0
  line_index = 0
  while lines:
    if line_index >= len(lines):
      line_index = 0
      call_index += 1
    else:
      board_index, line = lines[line_index]
      if board_index not in remaining:
        del lines[line_index]
      else:
        call = calls[call_index]
        if call in line:
          line.remove(call)
          if len(line) == 0:
            if first_bingo is None:
              first_bingo = (board_index, call_index)
            if len(remaining) == 1:
              last_bingo = (board_index, call_index)
            remaining.remove(board_index)
            del lines[line_index]
          else:
            line_index += 1
        else:
          line_index += 1

  for board_index, call_index in (first_bingo, last_bingo):
    called = set(calls[:call_index + 1])
    uncalled = 0
    for row in boards[board_index]:
      for col in row:
        if col not in called:
          uncalled += col
    yield uncalled * calls[call_index]

if __name__ == "__main__":
  p.main()
