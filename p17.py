#!/usr/bin/env python3

import itertools

import p

@p.puzzle(17, 1)
def main(lines):
  x_range, y_range = lines[0].split(" ")[2:]
  x_range = tuple(map(int, x_range[2:-1].split("..")))
  y_range = tuple(map(int, y_range[2:].split("..")))
  x0, x1 = min(x_range), max(x_range)
  y0, y1 = min(y_range), max(y_range)
  big_x = max(abs(x0), abs(x1))
  big_y = max(abs(y0), abs(y1))

  dx0s = {}
  stops_in = {}
  for dx0 in range(-big_x, big_x + 1):
    x = 0
    dx = dx0
    hit_iterations = set()
    for iteration in itertools.count():
      if x < -big_x or big_x < x:
        break
      if x0 <= x <= x1:
        hit_iterations.add(iteration)
        if dx == 0:
          stops_in[dx0] = iteration
      if dx == 0:
        break
      x += dx
      if 0 < dx:
        dx -= 1
      elif dx < 0:
        dx += 1
    if hit_iterations:
      dx0s[dx0] = hit_iterations

  dy0s = {}
  max_ys = {}
  for dy0 in range(-big_y, big_y + 1):
    y = 0
    dy = dy0
    hit_iterations = set()
    max_y = 0
    for iteration in itertools.count():
      if y < -big_y:
        break
      if y0 <= y <= y1:
        hit_iterations.add(iteration)
      y += dy
      dy -= 1
      max_y = max(max_y, y)
    if hit_iterations:
      dy0s[dy0] = hit_iterations
      max_ys[dy0] = max_y

  max_y = 0
  valids = 0
  for dx0, dy0 in itertools.product(dx0s.keys(), dy0s.keys()):
    if (dx0s[dx0] & dy0s[dy0] or
        (dx0 in stops_in and any(it >= stops_in[dx0] for it in dy0s[dy0]))):
      max_y = max(max_y, max_ys[dy0])
      valids += 1

  yield max_y
  yield valids

if __name__ == "__main__":
  p.main()
