#!/usr/bin/env python3

import itertools

import p

@p.puzzle(6, 1)
def main(lines):
  fishes = {timer: 0 for timer in range(9)}
  for fish in map(int, lines[0].split(",")):
    fishes[fish] += 1
  for day in itertools.count(1):
    tomorrow = {timer: 0 for timer in range(9)}
    for timer, count in fishes.items():
      if timer == 0:
        tomorrow[8] += count
        tomorrow[6] += count
      else:
        tomorrow[timer - 1] += count
    fishes = tomorrow
    if day == 80:
      yield sum(fishes.values())
    elif day == 256:
      yield sum(fishes.values())
      break

if __name__ == "__main__":
  p.main()
