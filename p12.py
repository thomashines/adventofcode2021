#!/usr/bin/env python3

import collections

import p

def get_paths(edges, path, visited, small_twice, end):
  if path[-1] == end:
    yield path
  else:
    for successor in edges[path[-1]]:
      if (successor != "start" and
          (successor.isupper() or
           (successor not in visited) or
           ((successor in visited) and (not small_twice)))):
        successor_path = path + (successor,)
        successor_visited = visited | {successor}
        successor_small_twice = small_twice or (successor.islower() and (successor in visited))
        yield from get_paths(edges, successor_path, successor_visited, successor_small_twice, end)

@p.puzzle(12, 1)
def main(lines):
  caves = set()
  edges = collections.defaultdict(lambda: set())
  for line in lines:
    left, right = line.split("-")
    caves.add(left)
    caves.add(right)
    edges[left].add(right)
    edges[right].add(left)
  yield len(tuple(get_paths(edges, ("start",), set(), True, "end")))
  yield len(tuple(get_paths(edges, ("start",), set(), False, "end")))

if __name__ == "__main__":
  p.main()
