#!/usr/bin/env python3

import itertools

import p

ADJACENTS = (
  (-1, -1),
  (-1,  0),
  (-1,  1),
  ( 0, -1),
  ( 0,  1),
  ( 1, -1),
  ( 1,  0),
  ( 1,  1),
)

def flash(state, x, y):
  flashes = 0
  if 0 <= y < len(state):
    if 0 <= x < len(state[y]):
      if state[y][x] > 0:
        state[y][x] += 1
        if state[y][x] > 9:
          state[y][x] = 0
          flashes = 1
          for dx, dy in ADJACENTS:
            nx, ny = x + dx, y + dy
            flashes += flash(state, nx, ny)
  return flashes

@p.puzzle(11, 1)
def main(lines):
  flashes = 0
  previous = [list(map(int, line)) for line in lines]
  size = len(previous) * len(previous[0])
  for step in itertools.count(1):
    state = [[octopus + 1 for octopus in row] for row in previous]
    new_flashes = 0
    for y in range(len(state)):
      for x in range(len(state[y])):
        if state[y][x] > 9:
          new_flashes += flash(state, x, y)
    flashes += new_flashes
    if step == 100:
      yield flashes
    if new_flashes == size:
      yield step
      break
    previous = state

if __name__ == "__main__":
  p.main()
