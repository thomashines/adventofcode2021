#!/usr/bin/env python3

import p

@p.puzzle(1, 1)
def main(lines):
  depths = tuple(map(int, lines))

  count = 0
  previous = None
  for depth in depths:
    if previous is not None and depth > previous:
      count += 1
    previous = depth
  yield count

  previous_sum = 0
  previous_sum_size = 0
  next_sum = 0
  next_sum_size = 0
  count = 0
  for index, depth in enumerate(depths):
    next_sum = previous_sum + depth
    next_sum_size = previous_sum_size + 1
    if previous_sum_size == 3:
      next_sum -= depths[index - 3]
      next_sum_size -= 1
      if next_sum > previous_sum:
        count += 1
    previous_sum = next_sum
    previous_sum_size = next_sum_size
  yield count

if __name__ == "__main__":
  p.main()
