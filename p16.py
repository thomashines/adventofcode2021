#!/usr/bin/env python3

import p

def btoi(bits):
  value = 0
  for index, bit in enumerate(reversed(bits)):
    value += bit * (2 ** index)
  return value

def parse(bits):
  version = btoi(bits[0:3])
  type_id = btoi(bits[3:6])
  size = 6
  value = None
  if type_id == 4:
    index = 6
    literal_bits = []
    while index < len(bits):
      literal_bits.extend(bits[index + 1:index + 5])
      if bits[index] == 0:
        break
      index += 5
    size = index + 5
    value = btoi(literal_bits)
  else:
    length_type_id = bits[6]
    value_index = None
    length = None
    packets = None
    if length_type_id == 0:
      value_index = 22
      length = btoi(bits[7:22])
    elif length_type_id == 1:
      value_index = 18
      packets = btoi(bits[7:18])
    index = value_index
    value = []
    while True:
      packet = parse(bits[index:])
      index += packet["size"]
      value.append(packet)
      if packets is not None and len(value) == packets:
        break
      if length is not None and (index - value_index) >= length:
        break
    size = index
  return {
    "version": version,
    "type_id": type_id,
    "value": value,
    "size": size,
  }

def version_sum(packet):
  total = packet["version"]
  if packet["type_id"] != 4:
    total += sum(version_sum(child) for child in packet["value"])
  return total

def solve(packet):
  if packet["type_id"] == 0:
    return sum(solve(child) for child in packet["value"])
  elif packet["type_id"] == 1:
    return p.product(solve(child) for child in packet["value"])
  elif packet["type_id"] == 2:
    return min(solve(child) for child in packet["value"])
  elif packet["type_id"] == 3:
    return max(solve(child) for child in packet["value"])
  elif packet["type_id"] == 4:
    return packet["value"]
  elif packet["type_id"] == 5:
    left, right = (solve(child) for child in packet["value"])
    return 1 if left > right else 0
  elif packet["type_id"] == 6:
    left, right = (solve(child) for child in packet["value"])
    return 1 if left < right else 0
  elif packet["type_id"] == 7:
    left, right = (solve(child) for child in packet["value"])
    return 1 if left == right else 0
  return None

@p.puzzle(16, 1)
def main(lines):
  bits = []
  for hexadecimal in lines[0]:
    bits.extend(map(int, "{:04b}".format(int(hexadecimal, base=16))))
  bits = tuple(bits)
  packet = parse(bits)
  yield version_sum(packet)
  yield solve(packet)

if __name__ == "__main__":
  p.main()
