#!/usr/bin/env python3

import p

CACHE = {}
def play_quantum(positions, scores=(0, 0), turn=0, roll=0):
  global CACHE
  if (positions, scores, turn, roll) in CACHE:
    return CACHE[positions, scores, turn, roll]
  next_roll = (roll + 1) % 3
  next_turn = (1 - turn) if (next_roll == 0) else turn
  wins = [0, 0]
  for value in range(3):
    next_positions = list(positions)
    next_scores = list(scores)
    next_positions[turn] = (next_positions[turn] + value + 1) % 10
    if roll == 2:
      next_scores[turn] += (next_positions[turn] + 1)
    if (roll == 2) and (next_scores[turn] >= 21):
      wins[turn] += 1
    else:
      next_wins = play_quantum(tuple(next_positions), tuple(next_scores), next_turn, next_roll)
      wins[0] += next_wins[0]
      wins[1] += next_wins[1]
  wins = tuple(wins)
  CACHE[positions, scores, turn, roll] = wins
  return wins

@p.puzzle(21, 1)
def main(lines):
  global CACHE
  for key in tuple(CACHE.keys()):
    del CACHE[key]

  positions = []
  for line in lines:
    positions.append(int(line[-1]) - 1)
  original_positions = tuple(positions)

  rolls = 0
  turn = 0
  scores = [0, 0]
  while max(scores) < 1000:
    roll = (rolls % 100) + ((rolls + 1) % 100) + ((rolls + 2) % 100) + 3
    rolls += 3
    positions[turn] = (positions[turn] + roll) % 10
    scores[turn] += (positions[turn] + 1)
    turn = 1 - turn
  yield rolls * min(scores)

  yield max(play_quantum(original_positions))

if __name__ == "__main__":
  p.main()
