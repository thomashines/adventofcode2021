#!/usr/bin/env python3

import p

def ge(lines, alt_g="1", alg_e="0"):
  rates = {}
  for line in lines:
    for index, char in enumerate(line):
      if index not in rates:
        rates[index] = {"0": 0, "1": 0}
      rates[index][char] += 1
  gamma = ""
  epsilon = ""
  for index in sorted(rates.keys()):
    if rates[index]["0"] > rates[index]["1"]:
      gamma += "0"
      epsilon += "1"
    else:
      gamma += "1"
      epsilon += "0"
  return gamma, epsilon

@p.puzzle(3, 1)
def main(lines):
  gamma, epsilon = ge(lines)
  gamma = int(gamma, 2)
  epsilon = int(epsilon, 2)
  yield gamma * epsilon

  oxygens = set(lines)
  index = 0
  while len(oxygens) > 1:
    gamma, epsilon = ge(oxygens)
    for number in tuple(oxygens):
      if number[index] != gamma[index]:
        oxygens.remove(number)
    index += 1
  oxygen = int(next(iter(oxygens)), 2)

  co2s = set(lines)
  index = 0
  while len(co2s) > 1:
    gamma, epsilon = ge(co2s)
    for number in tuple(co2s):
      if number[index] != epsilon[index]:
        co2s.remove(number)
    index += 1
  co2 = int(next(iter(co2s)), 2)

  yield oxygen * co2

if __name__ == "__main__":
  p.main()
