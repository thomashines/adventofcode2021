#!/usr/bin/env python3

import itertools

import p

ADJACENTS = (
  (-1, -1),
  (0, -1),
  (1, -1),
  (-1, 0),
  (0, 0),
  (1, 0),
  (-1, 1),
  (0, 1),
  (1, 1),
)

@p.puzzle(20, 1)
def main(lines):
  algorithm = tuple(character == "#" for character in lines[0])
  image = {}
  min_x = 0
  min_y = 0
  max_x = 0
  max_y = 0
  for y, line in enumerate(lines[2:]):
    for x, character in enumerate(line):
      image[x, y] = character == "#"
      min_x = min(min_x, x)
      max_x = max(max_x, x)
      min_y = min(min_y, y)
      max_y = max(max_y, y)
  default = False

  for step in itertools.count(start=1):
    next_image = {}
    for y in range(min_y - 1, max_y + 2):
      for x in range(min_x - 1, max_x + 2):
        value = 0
        for index, (dx, dy) in enumerate(reversed(ADJACENTS)):
          if image.get((x + dx, y + dy), default):
            value += (1 << index)
        next_image[x, y] = algorithm[value]
    default = algorithm[-1] if default else algorithm[0]
    image = next_image
    min_x -= 1
    max_x += 1
    min_y -= 1
    max_y += 1
    if step == 2:
      count = sum(1 if value else 0 for value in image.values())
      yield count
    if step == 50:
      count = sum(1 if value else 0 for value in image.values())
      yield count
      break

if __name__ == "__main__":
  p.main()
