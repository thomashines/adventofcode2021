#!/usr/bin/env python3

import collections
import itertools

import p

def value_number(number):
  return {
    "type": "number",
    "value": number,
    "min": number,
    "max": number,
    "factors": set((number,)),
  }

def value_variable(variable):
  return {
    "type": "variable",
    "value": variable,
    "min": 1,
    "max": 9,
    "factors": set(),
  }

def value_addition(children):
  return {
    "type": "operand",
    "value": "+",
    "children": tuple(children),
    "min": sum(child["min"] for child in children),
    "max": sum(child["max"] for child in children),
    "factors": set(),
  }

def value_multiplication(children):
  factors = set()
  for child in children:
    factors = factors | child["factors"]
  return {
    "type": "operand",
    "value": "*",
    "children": tuple(children),
    "min": p.product(child["min"] for child in children),
    "max": p.product(child["max"] for child in children),
    "factors": factors,
  }

def value_division(numerator, denominator):
  return {
    "type": "operand",
    "value": "/",
    "children": (numerator, denominator),
    "min": numerator["min"] // denominator["max"],
    "max": numerator["max"] // denominator["min"],
    "factors": set(),
  }

def value_modulus(numerator, denominator):
  return {
    "type": "operand",
    "value": "%",
    "children": (numerator, denominator),
    "min": 0,
    "max": max(0, denominator["max"] - 1),
    "factors": set(),
  }

def value_equality(left, right):
  return {
    "type": "operand",
    "value": "=",
    "children": (left, right),
    "min": 0,
    "max": 1,
    "factors": set(),
  }

def stringify(value):
  if value["type"] == "variable":
    return "in{:>02}".format(value["value"])
  if value["type"] == "number":
    return str(value["value"])
  return "({})".format(" {} ".format(value["value"]).join(map(stringify, value["children"])))

def simplify(value):
  if value["type"] in ("number", "variable"):
    return value

  children = tuple(map(simplify, value["children"]))

  if value["value"] == "+":

    flattened = []
    for child in children:
      if child["value"] == "+":
        flattened.extend(child["children"])
      else:
        flattened.append(child)

    reduced = []
    constant_value = 0
    for child in flattened:
      if child["min"] == child["max"]:
        constant_value += child["min"]
      else:
        reduced.append(child)
    if constant_value != 0:
      reduced.append(value_number(constant_value))

    if len(reduced) == 1:
      return reduced[0]

    value = value_addition(reduced)

  elif value["value"] == "*":

    flattened = []
    for child in children:
      if child["value"] == "*":
        flattened.extend(child["children"])
      else:
        flattened.append(child)

    reduced = []
    constant_value = 1
    for child in flattened:
      if child["min"] == child["max"]:
        if child["min"] == 0:
          return value_number(0)
        constant_value *= child["min"]
      else:
        reduced.append(child)
    if constant_value != 1:
      reduced.append(value_number(constant_value))

    if len(reduced) == 1:
      return reduced[0]

    value = value_multiplication(reduced)

  elif value["value"] == "/":

    numerator = children[0]
    denominator = children[1]

    if 0 == numerator["min"] == numerator["max"]:
      return value_number(0)

    if 1 == denominator["min"] == denominator["max"]:
      return children[0]

    if denominator["min"] == denominator["max"]:
      denominator_value = denominator["min"]

      if numerator["value"] == "+":
        numerator_children = []
        for child in numerator["children"]:
          if child["max"] >= denominator_value:
            numerator_children.append(child)
        if len(numerator_children) == 0:
          return value_number(0)
        elif len(numerator_children) == 1:
          numerator = numerator_children[0]
        else:
          numerator = value_addition(numerator_children)

      if numerator["value"] == "*":
        found_denominator = False
        numerator_children = []
        for index, child in enumerate(numerator["children"]):
          if denominator_value == child["min"] == child["max"]:
            found_denominator = True
            numerator_children.extend(numerator["children"][index + 1:])
            break
          else:
            numerator_children.append(child)
        if len(numerator_children) == 0:
          if found_denominator:
            return value_number(1)
          return value_number(0)
        elif len(numerator_children) == 1:
          if found_denominator:
            return numerator_children[0]
          numerator = numerator_children[0]
        elif found_denominator:
          numerator = value_multiplication(numerator_children)
          return numerator

    value = value_division(numerator, denominator)

  elif value["value"] == "%":

    numerator = children[0]
    denominator = children[1]

    if denominator["min"] == denominator["max"]:
      denominator_value = denominator["min"]

      if numerator["min"] == numerator["max"]:
        numerator_value = numerator["min"]
        return value_number(numerator_value % denominator_value)

      if denominator_value in numerator["factors"]:
        return value_number(0)

      if numerator["value"] == "+":
        numerator_children = []
        for child in numerator["children"]:
          if denominator_value not in child["factors"]:
            numerator_children.append(child)
        numerator = value_addition(numerator_children)

    if numerator["max"] < denominator["min"]:
      return numerator

    value = value_modulus(numerator, denominator)

  elif value["value"] == "=":

    left = children[0]
    right = children[1]

    if ((left["max"] < right["min"]) or
        (right["max"] < left["min"])):
      return value_number(0)

    if ((left["min"] == left["max"]) and
        (right["min"] == right["max"])):
      return value_number(1 if (left["min"] == right["min"]) else 0)

    value = value_equality(left, right)

  if value["min"] == value["max"]:
    return value_number(value["min"])

  return value

def get_argument_value(variables, argument):
  if argument in "wxyz":
    return variables[argument]
  return value_number(int(argument))

def get_equalities(value):
  has_child_equalities = False
  for child in value.get("children", ()):
    child_equalities = tuple(get_equalities(child))
    if child_equalities:
      has_child_equalities = True
      yield from child_equalities
  if not has_child_equalities and value["value"] == "=":
    yield value

def get_rules(value):
  for equality_value in get_equalities(value):
    if equality_value["value"] == "=":
      left, right = equality_value["children"]
      if left["value"] == "+":
        left_variable = left["children"][0]
        left_constant = left["children"][1]
        if ((left_variable["type"] == "variable") and
            (left_constant["type"] == "number")):
          yield (left_variable["value"], left_constant["value"], right["value"])
          yield (right["value"], -left_constant["value"], left_variable["value"])

def set_variables(value, variables):
  if value["type"] == "variable":
    if value["value"] in variables:
      return value_number(variables[value["value"]])
    return value_variable(value["value"])
  elif value["type"] == "number":
    return value_number(value["value"])
  else:
    children = tuple(map(lambda child: set_variables(child, variables), value["children"]))
    if value["value"] == "+":
      return value_addition(children)
    elif value["value"] == "*":
      return value_multiplication(children)
    elif value["value"] == "/":
      return value_division(*children)
    elif value["value"] == "%":
      return value_modulus(*children)
    elif value["value"] == "=":
      return value_equality(*children)
  return None

@p.puzzle(24, 1)
def main(lines):
  program = tuple(tuple(line.split(" ")) for line in lines)

  variables = collections.defaultdict(lambda: value_number(0))
  input_index = 0
  for instruction in program:
    command = instruction[0]
    arguments = instruction[1:]
    values = tuple(map(lambda argument: get_argument_value(variables, argument),
                       arguments))
    if command == "inp":
      variables[arguments[0]] = value_variable(input_index)
      input_index += 1
    elif command == "add":
      variables[arguments[0]] = simplify(value_addition(values))
    elif command == "mul":
      variables[arguments[0]] = simplify(value_multiplication(values))
    elif command == "div":
      variables[arguments[0]] = simplify(value_division(*values))
    elif command == "mod":
      variables[arguments[0]] = simplify(value_modulus(*values))
    elif command == "eql":
      variables[arguments[0]] = simplify(value_equality(*values))

  lowest = 99999999999999
  highest = 11111111111111

  checked_rules = set()
  checked_low_serials = set()
  checked_high_serials = set()
  rules = set(get_rules(variables["z"]))
  while True:
    new_rules = set()
    for try_rules_count in range(1, len(rules) + 1):
      for try_rules in itertools.combinations(rules, try_rules_count):
        try_rules = tuple(sorted(try_rules))
        if try_rules in checked_rules:
          continue
        checked_rules.add(try_rules)

        low_serial = {}
        low_possible = True
        for left_index, difference, right_index in reversed(try_rules):
          if (left_index in low_serial) and (right_index in low_serial):
            if (low_serial[left_index] + difference) != low_serial[right_index]:
              low_possible = False
              break
          elif left_index in low_serial:
            low_serial[right_index] = low_serial[left_index] + difference
          elif right_index in low_serial:
            low_serial[left_index] = low_serial[right_index] - difference
          elif difference < 0:
            low_serial[right_index] = 1
            low_serial[left_index] = 1 - difference
          elif difference == 0:
            low_serial[left_index] = low_serial[right_index] = 1
          elif 0 < difference:
            low_serial[left_index] = 1
            low_serial[right_index] = 1 + difference
          if low_serial[right_index] < 1 or 9 < low_serial[right_index]:
            low_possible = False
            break

        high_serial = {}
        high_possible = True
        for left_index, difference, right_index in try_rules:
          if (left_index in high_serial) and (right_index in high_serial):
            if (high_serial[left_index] + difference) != high_serial[right_index]:
              high_possible = False
              break
          elif left_index in high_serial:
            high_serial[right_index] = high_serial[left_index] + difference
          elif right_index in high_serial:
            high_serial[left_index] = high_serial[right_index] - difference
          elif difference < 0:
            high_serial[left_index] = 9
            high_serial[right_index] = 9 + difference
          elif difference == 0:
            high_serial[left_index] = high_serial[right_index] = 9
          elif 0 < difference:
            high_serial[right_index] = 9
            high_serial[left_index] = 9 - difference
          if high_serial[right_index] < 1 or 9 < high_serial[right_index]:
            high_possible = False
            break

        low_serial_flat = tuple(sorted(low_serial.items()))
        low_serial_int = int("".join(str(low_serial.get(index, 1)) for index in range(14)))
        if low_possible and (low_serial_int < lowest) and (low_serial_flat not in checked_low_serials):
          checked_low_serials.add(low_serial_flat)
          z_value = simplify(set_variables(variables["z"], low_serial))
          if 0 == z_value["min"] == z_value["max"]:
            lowest = low_serial_int
          else:
            new_rules.update(get_rules(z_value))

        high_serial_flat = tuple(sorted(high_serial.items()))
        high_serial_int = int("".join(str(high_serial.get(index, 1)) for index in range(14)))
        if high_possible and (highest < high_serial_int) and (high_serial_flat not in checked_high_serials):
          checked_high_serials.add(high_serial_flat)
          z_value = simplify(set_variables(variables["z"], high_serial))
          if 0 == z_value["min"] == z_value["max"]:
            highest = high_serial_int
          else:
            new_rules.update(get_rules(z_value))

    if len(new_rules - rules) == 0:
      break

    rules.update(new_rules)

  yield lowest
  yield highest

if __name__ == "__main__":
  p.main()
